const express = require('express');
const request = require('request');
const app = express();

const {MAIN_NODE, DEFAULT_PORT} = require('./bc/utils/Globals');

const rootRouter = require('./serverside/rootRouter');
const {syncHub, bcRouter} = require('./serverside/bcRouter');

const randomPort = DEFAULT_PORT + Math.ceil(Math.random() * 100);
let currentPort = process.env.PORT || (process.env.PEER === 'true' ? randomPort : DEFAULT_PORT);

app.set('view engine', 'pug');
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.use('/', rootRouter);
app.use('/bc', bcRouter);
app.use((req, res, next)=>{
    res.status(404);
    res.render('notfound', {
        wrongturn: req.originalUrl
    });
});
const sync = ()=>{
    let url = MAIN_NODE + '/bc/getchain';
    request({
        url: url,
        method: "POST"
    }, (error, response)=>{
        if(!error && response.statusCode){
            const rootChain = JSON.parse(response.body);
            syncHub.blockchain.setChain(rootChain);
        }
    });
};

app.listen(currentPort, ()=>{
    console.log(`Server started at http://localhost:${currentPort}`);
    if(currentPort !== DEFAULT_PORT) sync();
});