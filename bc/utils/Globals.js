const DEV_MODE = true;

const MINE_INTERVAL = 1000;
const GENESIS_DATA  = {
    prevHash: 'origin',
    timestamp: 1582731591951,
    data: {
        message: 'The secret to getting ahead is getting started - Mark Twain'
    },
    salt: 0,
    difficulty: 1,
    hash: 'b5144d3b1be6ca16c6b470de4913cfb59d5a2f569c4040a79a1df556efe52e54'
};

const PIPE_DATA = {
    DEFAULT: "BLOCKCHAIN"
};

const DEFAULT_PORT = 3000;
const MAIN_NODE = `http://localhost:${DEFAULT_PORT}`;

const REDIS_PATH = 'redis://127.0.0.1:6379';

const MOCK_USER_DATA = {
    salt: 'foobar',
    userlist: [
        {
            name: 'Harry Potter',
            username: 'harryp',
            email: 'harryp@hogwarts.com',
            password: 'Hedwig'
        },
        {
            name: 'Ron Weasley',
            username: 'ronw',
            email: 'ronw@hogwarts.com',
            password: 'Scabbers'
        },
        {
            name: 'Hermione Granger',
            username: 'hermioneg',
            email: 'hermioneg@hogwarts.com',
            password: 'Crookshanks'
        }
    ]
};

module.exports = {
    MINE_INTERVAL,
    GENESIS_DATA,
    PIPE_DATA,
    MAIN_NODE,
    DEFAULT_PORT,
    REDIS_PATH,
    MOCK_USER_DATA,
    DEV_MODE
};