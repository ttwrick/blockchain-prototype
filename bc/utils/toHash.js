const crypto = require('crypto');

const toHash = (...input) => {
    const hash = crypto.createHash('sha256');
    const result = input.sort().join(' ');
    hash.update(result);
    return hash.digest('hex');
};

module.exports = toHash;