const toHash = require('./toHash');
const {GENESIS_DATA} = require('./Globals');

describe("toHash", ()=>{
    test('Converts a string array to a hash', ()=>{
        expect(toHash('b', 'a', 'c')).toBe('0e9f64031fcb2bc708b531c2a20441580425d151a38503f38592a7dd36019d3b');
    });

    test("returns without error when called with real args", ()=>{
        const mocker = jest.fn(()=>{
            let proxy = GENESIS_DATA;
            let hash = toHash(proxy.prevHash, proxy.timestamp, proxy.data, proxy.salt, proxy.difficulty);
        });
        mocker();

        expect(mocker).toHaveReturned();
    });
})