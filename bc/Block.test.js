const hexToBin = require('hex-to-binary');

const Block = require('./Block');
const toHash = require('./utils/toHash');

describe("Blocks", ()=>{
    const timestamp = 12345;
    const prevHash = 'prevHash';
    const data = {message: 'Adam and Eve'};
    const difficulty = 1;
    const salt = 0;
    const hash = toHash(prevHash, timestamp, data, difficulty, salt);
    const genBlock = new Block({
        timestamp,
        prevHash,
        hash,
        data,
        difficulty,
        salt
    });
    
    test("should have a `timestamp` property", ()=>{
        expect(Reflect.has(genBlock, 'timestamp')).toBe(true);
    });

    test("should have a `prevHash` property", ()=>{
        expect(Reflect.has(genBlock, 'prevHash')).toBe(true);
    });
    
    test("should have a `hash` property", ()=>{
        expect(Reflect.has(genBlock, 'hash')).toBe(true);
    });

    describe("data", ()=>{
        test("should exist", ()=>{
            expect(Reflect.has(genBlock, 'data')).toBe(true);
        });

        test("should have a `message` property", ()=>{
            expect(Reflect.has(genBlock.data, 'message')).toBe(true);
        });
    });

    test("should have a `difficulty` property", ()=>{
        expect(Reflect.has(genBlock, 'difficulty')).toBe(true);
    });

    test("should have a `salt` property", ()=>{
        expect(Reflect.has(genBlock, 'salt')).toBe(true);
    });

    describe("verifyHash()", ()=>{
        test("should return true if a block's hash is correct", ()=>{
            expect(Block.verifyHash(genBlock)).toBe(true);
        });

        test("should return false if a block's hash is not the same", ()=>{
            genBlock.hash = "anotherHash";
            expect(Block.verifyHash(genBlock)).toBe(false);
        });
    });

    describe("updateDiff()", ()=>{
        const newBlock = new Block({
            prevHash: 'none',
            timestamp: Date.now(),
            data: {
                message: 'toTestDiff'
            },
            salt: 0,
            difficulty: 1,
            hash: 'placeholder'
        });
        newBlock.hash = toHash(newBlock.prevHash, newBlock.timestamp, newBlock.data, newBlock.salt, newBlock.difficulty);
        test("should returned a different difficulty than the last block's", ()=>{
            expect(Block.updateDiff(newBlock, Date.now())).not.toBe(newBlock.difficulty);
        });
    });

    describe("mineBlock()", ()=>{
        test("should return a new block", ()=>{
            expect(Block.mineBlock(genBlock, {message: 'any'})).toBeInstanceOf(Block);
        });

        test("return should have a `difficulty` number of trailing 0's", ()=>{
            const returnedBlock = Block.mineBlock(genBlock, {message: 'anymore'});
            expect(hexToBin(returnedBlock.hash).substring(0, returnedBlock.difficulty))
                .toEqual('0'.repeat(returnedBlock.difficulty));
        });
    });

    describe("getGenesis()", ()=>{
        test("should return a new genesis block", ()=>{
            expect(Block.getGenesis()).toBeInstanceOf(Block);
        });

        test("should instantiate with all it's properties initialized", ()=>{
            let proxyGen = Block.getGenesis();
            expect(proxyGen.prevHash).toBeTruthy();
            expect(proxyGen.timestamp).toBeTruthy();
            expect(proxyGen.data).toBeTruthy();
            expect(proxyGen.salt).toBe(0);
            expect(proxyGen.difficulty).toBeTruthy();
            expect(proxyGen.hash).toBeTruthy();
        });
    });
});