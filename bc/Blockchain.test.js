const Blockchain = require('./Blockchain');
const Block = require('./Block');
const toHash = require('./utils/toHash');
const {GENESIS_DATA} = require('./utils/Globals');

describe("Blockchain", ()=>{
    const newBC = new Blockchain();
    describe("getChain()", ()=>{
        test("Returns a non-empty array instance", ()=>{
            expect(newBC.getChain()).toBeInstanceOf(Array);
            expect(newBC.getChain().length > 0).toBe(true);
        });
    });

    describe("setChain()", ()=>{
        let tempBlock;
        let chainToCompare;
        beforeEach(()=>{
            tempBlock = new Block({
                prevHash: GENESIS_DATA.hash,
                timestamp: Date.now(),
                data: {
                    message: "any"
                },
                salt: 0,
                difficulty: 2
            });
            tempBlock.hash = toHash(tempBlock.prevHash, tempBlock.timestamp, tempBlock.data, tempBlock.salt, tempBlock.difficulty);
            chainToCompare = newBC.getChain();
            chainToCompare.push(tempBlock);
        });
        test("The new chain returns an error if it's not longer", ()=>{
            Blockchain.validateChain(newBC.getChain(), newBC.getChain(), (error)=>{
                expect(error).toBeTruthy();
            });
        });

        test("The new chain returns an error if it has an invalid block hash", ()=>{
            chainToCompare[chainToCompare.length - 1].hash = 'invalidHash';
            Blockchain.validateChain(chainToCompare, newBC.getChain(), (error)=>{
                expect(error).toBeTruthy();
            });
        });

        test("The new chain returns an error if not all nodes are linked through `prevHash`", ()=>{
            chainToCompare[chainToCompare.length - 1].prevHash = 'invalid';
            Blockchain.validateChain(chainToCompare, newBC.getChain(), (error)=>{
                expect(error).toBeTruthy();
            });
        });

        test("The new chain returns an error if there are difficulty jumps", ()=>{
            chainToCompare[chainToCompare.length - 1].difficulty = 9;
            Blockchain.validateChain(chainToCompare, newBC.getChain(), (error)=>{
                expect(error).toBeTruthy();
            });
        });
    });

    describe("getChain()", ()=>{
        const bc = new Blockchain();
        let orphanChain = [Block.getGenesis()];
        test("On a fresh instance, should return a chain with only genesis block", ()=>{
            expect(bc.getChain()).toEqual(orphanChain);
        });

        test("Should return a copy and NOT a reference", ()=>{
            const newBlock = new Block({
                prevHash: 'mock',
                hash: 'data',
                timestamp: 121,
                data: {
                    message: 'foo'
                },
                salt: 0,
                difficulty: 1
            });
            let copy = bc.getChain();
            copy.push(newBlock);
            expect(bc.getChain()).not.toEqual(copy);
        });
    });

    describe("pushBlock()", ()=>{
        test("Should increase the chain length", ()=>{
            let origiLen = newBC._chain.length;
            newBC.pushBlock({message: "pushTest"});
            let newLen = newBC._chain.length;
            expect(origiLen < newLen).toBe(true);

        });

        test("The new block must contain the data passed in the args", ()=>{
            let tempData = {message: "here's jhonny!"};
            newBC.pushBlock(tempData);
            let lastBlock = newBC._chain[newBC._chain.length - 1];
            expect(lastBlock.data.message === tempData.message).toEqual(true);
        });

        test("The new length of the chain must be returned", ()=>{
            let tempData = {message: "here's jhonny!"};
            let origiLen = newBC._chain.length;
            let newLen = newBC.pushBlock(tempData);
            expect(newLen > origiLen).toBe(true);
        });
    });

    describe("popBlock()", ()=>{
        test("Should return the last element in the chain", ()=>{
            let lastElement = newBC._chain[newBC._chain.length - 1];
            expect(newBC.popBlock()).toEqual(lastElement);
        });

        test("Should NOT return different elements on consecutive calls", ()=>{
            let firstCall = newBC.popBlock();
            let secondCall = newBC.popBlock();
            expect(firstCall).toEqual(secondCall);
        });
    });

    describe("currentLen()", ()=>{

        test("Should return an integer", ()=>{
            expect(typeof(newBC.currentLen())).toEqual('number');
        });

        test("Should return the current length of the chain", ()=>{
            expect(newBC.currentLen()).toEqual(newBC._chain.length);
        });
    });
});