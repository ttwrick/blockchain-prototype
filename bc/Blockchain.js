const Block = require('./Block');

class Blockchain{
    constructor(){
        this._chain = [Block.getGenesis()];
    }

    getChain(){
        return this._chain.slice();
    }

    setChain(newChain){
        Blockchain.validateChain(newChain, this._chain, (error)=>{
            if(error) console.error(error);
            else this._chain = newChain;
        });
    }

    //ctVal - Chain to validate _ returns errorMsg
    static validateChain(ctVal, oChain, callback){
        let errorMsg = undefined;
        if(ctVal.length <= oChain.length) errorMsg = "The new chain isn't longer";
        for(let i = 1, j = 0; i < ctVal.length; i++, j++){
            if(ctVal[i].prevHash !== ctVal[j].hash){
                errorMsg = "Not all nodes are linked";
                break;
            }
            
            if(!Block.verifyHash(ctVal[i])){
                errorMsg = "Not all block hashes are valid";
                break;
            }
            
            if(Math.abs(ctVal[i].difficulty - ctVal[j].difficulty) !== 1){
                errorMsg = "There are difficuty jumps";
                break;
            }
        }
        callback(errorMsg);
    }

    pushBlock(data){
        const lastBlock = this.popBlock();
        const minedBlock = Block.mineBlock(lastBlock, data);
        return this._chain.push(minedBlock);
    }

    popBlock(){
        return this._chain[this._chain.length - 1];
    }

    currentLen(){
        return this._chain.length;
    }
}

module.exports = Blockchain;