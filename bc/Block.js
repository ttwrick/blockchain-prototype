const hexToBin = require('hex-to-binary');

const toHash = require('./utils/toHash');
const {MINE_INTERVAL, GENESIS_DATA} = require('./utils/Globals');

class Block{
    constructor({prevHash, hash, timestamp, data, salt, difficulty}){
        this.prevHash = prevHash;
        this.timestamp = timestamp;
        this.data = data;
        this.salt = salt;
        this.difficulty = difficulty;
        this.hash = hash;
    }

    static verifyHash(block){
        const generatedHash = toHash(block.prevHash, block.timestamp, block.data, block.salt, block.difficulty);
        return block.hash === generatedHash;
    }

    static updateDiff(prevBlock, timestamp){
        const {difficulty} = prevBlock;
        if(difficulty < 1) return 1;
        if(timestamp - prevBlock.timestamp < MINE_INTERVAL) return difficulty + 1;
        return difficulty - 1;
    }

    static mineBlock(lastBlock, data){
        const lastHash = lastBlock.hash;
        let currTimestamp, currHash, difficulty, salt = 0;
        do{
            salt++;
            currTimestamp = Date.now();
            difficulty = this.updateDiff(lastBlock, currTimestamp);
            currHash = toHash(currTimestamp, lastHash, data, difficulty, salt);
        }while(hexToBin(currHash).substring(0, difficulty) !== '0'.repeat(difficulty));

        return new this({
            timestamp: currTimestamp,
            salt,
            difficulty,
            hash: currHash,
            prevHash: lastHash,
            data
        });
    }

    static getGenesis(){
        return new this({
            ...GENESIS_DATA
        });
    }
}
module.exports = Block;