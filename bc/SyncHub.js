const redis = require('redis');

const {PIPE_DATA, REDIS_PATH} = require('./utils/Globals');

class SyncHub{
    constructor(blockchain){
        this.blockchain =  blockchain;
        this.sender = redis.createClient(REDIS_PATH);
        this.receiver = redis.createClient(REDIS_PATH);

        this.receiver.subscribe(PIPE_DATA.DEFAULT);

        this.receiver.on("message", (channel, message)=>{
            this.messageIncoming(message);
        });
    }

    messageIncoming(message){
        const messageObj = JSON.parse(message);
        this.blockchain.setChain(messageObj);
    }

    sendMessage(message){
        this.receiver.unsubscribe(PIPE_DATA.DEFAULT, ()=>{
            this.sender.publish(PIPE_DATA.DEFAULT, message, ()=>{
                this.receiver.subscribe(PIPE_DATA.DEFAULT);
            });
        });
    }

    broadcastChain(){
        this.sendMessage(JSON.stringify(this.blockchain.getChain()));
    }
}

module.exports = SyncHub;