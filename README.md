# Blockchain JS prototype
## Description
This is a blockchain prototype written entirely in JavaScript using NodeJS, Redis (memurai is recommended as a substitute in Windows systems) and SQLite. The use case is the sharing of numbers through block's data and a registry of resource access.

## Requirements
For the project to be able to run, the following software needs to be installed:

* NodeJS & npm
* Redis (memurai in Windows systems)
* Git

*redis-server* in particular should be running in the background when the project executes.

## Usage
### First time usage
To clone the repository, create a directory where you want to store it and then from a terminal within the new directory run the following command:
`git clone https://bitbucket.org/ttwrick/blockchain-prototype.git`

After cloning, the installation of the dependencies is done with the following command (also in the terminal):
`npm install`

### How to run
*The following commands must be entered in a terminal running in the root directory of the project.*
To run the main application:
`npm start`
To run a replica listening to a different port:
`npm run replica`

**Note:** To run in a server, the MAIN_NODE and REDIS_PATH constants should be updated in the ./bc/utils/Global module
## Roadmap
The following is a checklist of milestones within the project:

- [x] A `Block` class.
    - [x] Should contain `timestamp`, `data`, `hash` and `previousHash` properties.
    - [x] Should contain `difficulty` and `salt` properties.
    - [x] Should have a static method to verify a block's hash.
    - [x] Should have a `updateDiff()` method.
    - [x] Should have a static `mineBlock()` method.
    - [x] Should have a static `getGenesis()` method.
- [x] A `Blockchain` class.
    - [x] Should contain a `chain` array of blocks.
        - [x] The chain should be a ~~private~~ pseudo-private field.
    - [x] Should contain a `getChain()` method.
    - [x] Should contain a `setChain()` method.
    - [x] Should have a static `verifyChainIntegrity()` method.
    - [x] Should have a `pushBlock()` method.
    - [x] Should have a `popBlock()` method.
    - [x] Should have a `currenLen()` method.
- [x] An express server
    - [x] Should listen to a development port 3000
        - [x] Should show a message to the console when running
    - [x] Should be able to create development peers on ports between 3000 and 3999
    - [x] Should implement a default message to any bad endpoints
    - [x] Should create a new random port for testing multiple instances
    - [x] Should contain a GET endpoint for the root path '/'
    - [x] Should contain a GET endpoint to show the blockchain
        - [x] The server should listen to that endpoint
        - [x] The endpoint should execute related logic and return an answer
    - [x] Should contain a POST endpoint to mine a block into the blockchain
        - [x] The server should listen to that endpoint
        - [x] The endpoint should execute related logic and return an answer
    - [x] Should implement a synchronization algorithm to update chains between nodes
- [x] `Registry` basic support
    - [x] Should have a login
        - [x] Basic session support
    - [x] Should have new endpoints only accessible by members
        - [x] Member's dashboard
        - [x] Resource endpoints
    - [x] Should push a new block to the chain with the accessed resource information

## Possible improvements
- [ ] `Blockchain`
    - [ ] A serialization method to have persistent data (FileSystem or Database)
- [ ] `SyncHub`
    - [ ] Replacing the Redis channels for a Best-Effort-Broadcast algorithm (or better)
- [ ] Transactions
    - [ ] Add the minimum capacity for transactions
    - [ ] Add transaction pools
- [ ] Miscelaneous
    - [ ] Update the way the mine request is done with resource access so it no longer uses de main node devPort by default