const sqlite3 = require('sqlite3').verbose();

const toHash = require('../../bc/utils/toHash');

const {MOCK_USER_DATA} = require('../../bc/utils/Globals');

const dbHandle = new sqlite3.Database(':memory:', (err)=>{
    if(err){
        console.error(err.message);
        return;
    }
    dbHandle.run(`CREATE TABLE user(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        name TEXT,
        username TEXT,
        email TEXT,
        password TEXT,
        UNIQUE(email)
    )`, (err)=>{
        if(err) console.error(err);
        else {
            const {userlist} = MOCK_USER_DATA;
            let stmt = 'INSERT INTO user(name, username, email, password) VALUES(?,?,?,?)';
            userlist.forEach((value)=>{
                dbHandle.run(
                    stmt, 
                    [value.name, 
                        value.username, 
                        value.email, 
                        toHash(value.password, MOCK_USER_DATA.salt)
                    ]);
            });
        }
    });
});

module.exports = dbHandle;