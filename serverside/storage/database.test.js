const dbHandler = require('./database');

describe('database', ()=>{
    test('exists and has rows already', (done)=>{
        dbHandler.serialize(()=>{
            dbHandler.all('SELECT * from user', (error, rows)=>{
                if(error) done(error);
                data = rows;
                try{
                    expect(rows).toBeTruthy();
                    done();
                } catch(throwable){
                    done(throwable);
                }
            });
        });
    });
});