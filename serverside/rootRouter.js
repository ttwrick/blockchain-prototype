const express = require('express');
const path = require('path');
const session = require('express-session');
const SQLiteStore = require('connect-sqlite3')(session);
const uuid4 = require('uuid').v4;
const request = require('request');

const router = express.Router();

const dbHandler = require('./storage/database');
const toHash = require('../bc/utils/toHash');
const {MOCK_USER_DATA, DEV_MODE, DEFAULT_PORT} = require('../bc/utils/Globals');

router.use(session({
    genid: () => uuid4(),
    secret: 'Griffindor',
    resave: false,
    saveUninitialized: false,
    cookie: {
        maxAge: 10 * 60 * 1000
    },
    store: new SQLiteStore
}));

router.use('/', express.static(path.resolve(__dirname, '../www/css')));
router.use('/', express.static(path.resolve(__dirname, '../www/js')));
router.use(express.json());
router.use(express.urlencoded({extended: true}));
router.get('/', (req, res, next)=>{
    res.status(200);
    res.render('index', {
        nada: 'none'
    });
    res.end();
});

router.get('/login', (req, res, next)=>{
    if(req.session.rowData) res.redirect('/member/' + req.session.rowData.username);
    else {
        res.status(200);
        res.header('Content-Type', 'text/html');
        res.render('login', {
            validationError: false
        });
        res.end();
    }
});

router.post('/login', (req, res, next)=>{
    const {email, pass} = req.body;
    dbHandler.get('SELECT * from user WHERE email=? AND password=?',
        [email, toHash(pass, MOCK_USER_DATA.salt)], 
        (error, row)=>{
            if(error) console.error(error);
            res.status(200);
            if(!row) {
                res.render('login', {
                    validationError: true
                });
            }
            else{
                req.session.pass = toHash(row.name, row.username);
                req.session.rowData = {
                    name: row.name,
                    username: row.username,
                    email: row.email
                };
                res.redirect('/member/'+ row.username);
            }
            res.end();
        }
    );
});

router.get('/member/:username', (req, res, next)=>{
    if(!req.session.rowData){
        res.redirect('/login');
        return;
    }
    res.render('memberdash', {
        title: `${req.session.rowData.username}'s Dashboard`,
        resxlist: [
            "/resource1",
            "/resource2",
            "/resource3"
        ]
    });
    res.end();
});

router.get('/resource*', (req, res, next)=>{
    if(!req.session.rowData){
        res.redirect('/login');
        return;
    }
    let currentResx = path.basename(req.originalUrl);
    let reqBod = JSON.stringify({
        data: {
            usercode: req.session.pass,
            assetcode: currentResx,
            timestamp: Date.now()
        }
    });
    let reqPath = 'http://' + req.hostname + (DEV_MODE ? `:${DEFAULT_PORT}`: '') + '/bc/mineBlock';
    let reqOpt = {
        method: 'POST',
        url: reqPath,
        headers: {
            'Content-Type': 'application/json'
        },
        body: reqBod
    }

    request(reqOpt, (error, response)=>{
        if(error){
            console.error(error);
        }
        console.log(response);
    });

    res.render('resourcerep', {
        title: `Welcome to the ${currentResx} page`,
        resxname: currentResx
    });
});

module.exports = router;