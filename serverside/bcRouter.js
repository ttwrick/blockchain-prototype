const path = require('path');
const fs = require('fs');
const express = require('express');

const Blockchain = require('../bc/Blockchain');
const SyncHub = require('../bc/SyncHub');

const bcRouter = express.Router();
const blockchain = new Blockchain();
const syncHub = new SyncHub(blockchain);

bcRouter.use('/', express.static(path.resolve(__dirname, '../www/css')));
bcRouter.use('/', express.static(path.resolve(__dirname, '../www/js')));
bcRouter.use(express.json());
bcRouter.use(express.urlencoded({extended: true}));

const showchain = (req, res, next)=>{
    res.setHeader("Content-Type", "text/html");
    res.render('showchain', {
        blocks: syncHub.blockchain.getChain().reverse()
    });
    res.end();
};

const getchain = (req, res, next)=>{
    res.setHeader("Content-Type", "application/json");
    res.write(JSON.stringify(syncHub.blockchain.getChain()));
    res.end();
};

bcRouter.get('/showchain', showchain);

bcRouter.post('/getchain', getchain);

bcRouter.post('/proposechain', (req, res, next)=>{
    // PLACEHOLDER
    res.write("Endpoint should be /bc/proposechain");
    res.end();
});

bcRouter.post('/mineBlock', (req,res,next)=>{
    let data;
    if(req.is('json'))
        data = req.body.data;
    else
        data = {message: req.body.data};
    syncHub.blockchain.pushBlock(data);
    syncHub.broadcastChain();
    res.redirect('/bc/showchain');
});

bcRouter.get('/sendblock', (req, res, next)=>{
    res.render('sendblock', {
        nada: 'none'
    });
});

module.exports = {
    syncHub,
    bcRouter
};
